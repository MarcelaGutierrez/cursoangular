import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DestinoviajeComponent } from './destinoviaje/destinoviaje.component';
import { ListaDestinoComponent } from './lista-destino/lista-destino.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinoviajeComponent,
    ListaDestinoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
